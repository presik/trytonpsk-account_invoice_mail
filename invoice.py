# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.wizard import Wizard, StateTransition
from trytond.transaction import Transaction

__all__ = ['Invoice', 'InvoiceSendMail']


class Invoice:
    __metaclass__ = PoolMeta
    __name__ = 'account.invoice'
    mail_sended = fields.Boolean('Mail Sended', states={
            'readonly': True
        })

    @classmethod
    def send_mail_pdf(cls, invoice):
        pool = Pool()
        Template = pool.get('electronic.mail.template')
        templates = Template.search([])
        print("Sended Mail.......")
        if templates:
            template = templates[0]
            print template.name
            context = Transaction().context.copy()
            with Transaction().set_context(context):
                Template.render_and_send(template.id, [invoice])


class InvoiceSendMail(Wizard):
    'Invoice Send Mail'
    __name__ = 'account.invoice.send_mail'
    start_state = 'send_mail'
    send_mail = StateTransition()

    def transition_send_mail(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        ids = Transaction().context['active_ids']
        invoice = Invoice(ids[0])
        Invoice.send_mail_pdf(invoice)
        return 'end'
